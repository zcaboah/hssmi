package com.resourceApp.network;

import com.resourceApp.model.Company;
import com.resourceApp.model.Report;
import com.resourceApp.model.Response;
import com.resourceApp.model.StatusUpdate;
import com.resourceApp.model.User;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface RetrofitInterface {

    @POST("users")
    Observable<Response> register(@Body User user);

    @POST("authenticate")
    Observable<Response> login();

    @GET("users/{email}")
    Observable<User> getProfile(@Path("email") String email);

    @GET("companies/{companyObjectId}")
    Observable<Company> getCompanyProfile(@Path("companyObjectId") String companyObjectId);

    @GET("reports/{userObjectId}")
    Observable<ArrayList<Report>> getSentReports(@Path("userObjectId") String userObjectId);

    @PUT("users/{email}")
    Observable<Response> changePassword(@Path("email") String email, @Body User user);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordInit(@Path("email") String email);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordFinish(@Path("email") String email, @Body User user);

    @POST("reports/{email}")
    Observable<Response> sendReport(@Path("email") String email, @Body Report report);

    @PUT("drafts/new/{email}")
    Observable<Response> saveDraft(@Path("email") String email, @Body Report report);

    @PUT("drafts/update/{email}")
    Observable<Response> updateDraft(@Path("email") String email, @Body Report report);

    @PUT("drafts/delete/{email}")
    Observable<Response> deleteDrafts(@Path("email") String email, @Body ArrayList<String> draftIds);

    @GET("admin/open/{companyObjectId}")
    Observable<ArrayList<Report>> getOpenReports(@Path("companyObjectId") String companyObjectId);

    @GET("admin/closed/{companyObjectId}")
    Observable<ArrayList<Report>> getClosedReports(@Path("companyObjectId") String companyObjectId);

    @PUT("admin/addReportStatus/{reportObjectId}")
    Observable<Response> addReportStatus(@Path("reportObjectId") String reportObjectId, @Body StatusUpdate statusUpdate);

    @GET("admin/getReportStatusUpdates/{reportObjectId}")
    Observable<ArrayList<StatusUpdate>> getReportStatusUpdates(@Path("reportObjectId") String reportObjectId);
}
