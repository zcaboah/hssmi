package com.resourceApp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Osman on 19/01/2017.
 */

public class ImageUtils {

    public static String getS3ImagePath(String refId, Context activityContext){
        String mUserId = PreferenceManager.getDefaultSharedPreferences(activityContext)
                .getString(Constants.USER_ID,"");
        return (mUserId + "/" + refId + ".jpg");
    }

    public static String getS3CompanyThumbnailPath(Context activityContext){
        String mCompanyId = PreferenceManager.getDefaultSharedPreferences(activityContext)
                .getString(Constants.COMPANY_ID,"");
        return (mCompanyId + "/companyIcon.jpg");
    }

    public static File createImageFile(String fileName, Context applicationContext) throws Exception{
        File file = new File(getInternalImagePath(fileName, applicationContext));
        try{
            file.getParentFile().mkdirs();
        }catch(Exception e){}
        return file;
    }

    public static File createTempImageFile(Context applicationContext) throws Exception{
        return ImageUtils.createImageFile("temp.jpg", applicationContext);
    }

    public static String getInternalImagePath(String fileName, Context applicationContext){
        return applicationContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath()
                + "/" + fileName;
    }

    public static BitmapFactory.Options getBitmapOptions(ImageView imageView, String mCurrentPhotoPath){
        // Get the dimensions of the bitmap
        int ivThumbnailW = imageView.getWidth();
        int ivThumbnailH = imageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        if(ivThumbnailH != 0 && ivThumbnailW != 0) {
            int scaleFactor = Math.min(photoW / ivThumbnailW, photoH / ivThumbnailH);
            bmOptions.inSampleSize = scaleFactor;
        }
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        return bmOptions;
    }

    public static void setImageViewBitmap(ImageView iv, String mCurrentPhotoPath){
        BitmapFactory.Options bmOptions = ImageUtils.getBitmapOptions(iv,mCurrentPhotoPath);
        Bitmap bmImg = BitmapFactory.decodeFile(mCurrentPhotoPath,bmOptions);
        iv.setImageBitmap(bmImg);
        iv.setAdjustViewBounds(true);
    }

    public static File saveScaledPhotoToFile(String imageFilePath, Context applicationContext) {
        //Convert file to bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap photoBm = BitmapFactory.decodeFile(imageFilePath,bmOptions);
        //get its orginal dimensions
        int bmOriginalWidth = photoBm.getWidth();
        int bmOriginalHeight = photoBm.getHeight();
        double originalWidthToHeightRatio =  1.0 * bmOriginalWidth / bmOriginalHeight;
        double originalHeightToWidthRatio =  1.0 * bmOriginalHeight / bmOriginalWidth;
        //choose a maximum height
        int maxHeight = 1600;
        //choose a max width
        int maxWidth = 1600;
        //call the method to get the scaled bitmap
        photoBm = getScaledBitmap(photoBm, bmOriginalWidth, bmOriginalHeight,
                originalWidthToHeightRatio, originalHeightToWidthRatio,
                maxHeight, maxWidth);

        /**********THE REST OF THIS IS FROM Prabu's answer*******/
        //create a byte array output stream to hold the photo's bytes
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //compress the photo's bytes into the byte array output stream
        photoBm.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

        File f = null;
        try {
            //construct a File object to save the scaled file to
            f = ImageUtils.createTempImageFile(applicationContext);
            //create the file
            f.createNewFile();

            //create an FileOutputStream on the created file
            FileOutputStream fo = new FileOutputStream(f);
            //write the photo's bytes to the file
            fo.write(bytes.toByteArray());

            //finish by closing the FileOutputStream
            fo.close();
        } catch(Exception e){
            Log.d("Compression exception", e.toString());
        }
        return f;
    }

    private static Bitmap getScaledBitmap(Bitmap bm, int bmOriginalWidth, int bmOriginalHeight, double originalWidthToHeightRatio, double originalHeightToWidthRatio, int maxHeight, int maxWidth) {
        if(bmOriginalWidth > maxWidth || bmOriginalHeight > maxHeight) {

            if(bmOriginalWidth > bmOriginalHeight) {
                bm = scaleDeminsFromWidth(bm, maxWidth, bmOriginalHeight, originalHeightToWidthRatio);
            } else if (bmOriginalHeight > bmOriginalWidth){
                bm = scaleDeminsFromHeight(bm, maxHeight, bmOriginalHeight, originalWidthToHeightRatio);
            }

        }
        return bm;
    }

    private static Bitmap scaleDeminsFromHeight(Bitmap bm, int maxHeight, int bmOriginalHeight, double originalWidthToHeightRatio) {
        int newHeight = (int) Math.max(maxHeight, bmOriginalHeight * .55);
        int newWidth = (int) (newHeight * originalWidthToHeightRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

    private static Bitmap scaleDeminsFromWidth(Bitmap bm, int maxWidth, int bmOriginalWidth, double originalHeightToWidthRatio) {
        //scale the width
        int newWidth = (int) Math.max(maxWidth, bmOriginalWidth * .75);
        int newHeight = (int) (newWidth * originalHeightToWidthRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

}
