package com.resourceApp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.network.S3Conn;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.activity_ui_config.EditableReportUIConfig;
import com.resourceApp.model.Report;
import com.resourceApp.model.ReportField;
import com.resourceApp.model.ReportSection;
import com.resourceApp.model.SubForm;
import com.resourceApp.model.SubFormField;
import com.resourceApp.model.SubFormSection;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.ImageUtils;
import com.resourceApp.utils.SyncUtils;
import com.resourceApp.utils.Utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.Constants.PREV_VIEWED_MENU_ITEM_ID;


@RuntimePermissions
public abstract class EditableReportActivity extends AppCompatActivity {

    protected Context activityContext;
    protected boolean draftActivity;
    protected SubForm subForm;
    protected Bundle savedState = null;
    protected boolean createdStateInDestroyView;
    protected static final String IMAGE_VIEW_PATH_TAG = "iv_bundle";
    protected static final String IMAGE_VIEW_WIDTH = "iv_width";
    protected static final String IMAGE_VIEW_HEIGHT = "iv_height";
    protected String mCurrentPhotoPath;
    protected int ivThumbnailW = 0;
    protected int ivThumbnailH = 0;
    protected String reportRefNo;
    protected File photoFile;
    // <Key, Value> pair for MongoDB user reports collection
    protected EditableReportUIConfig nrv;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_report);
        initBundle(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(subForm.getCategoryTitle());
        initViewParams();
        initView();

        // Keeps the thumbnail set when device is rotated.
        if(mCurrentPhotoPath != null && ivThumbnailW != 0) {
            setThumbnail();
        }
    }

    // Sets activity context and draftActivity boolean for initView() to execute.
    public abstract void initViewParams();

    public void onSubmitPressedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to submit this report?");
        builder.setPositiveButton("Yes", (dialog, id) -> submitReport());
        builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void photoOptionsDialog(){
        // Check if picture has already been taken and thumbnail has been set
        // If so dialog to remove existing photo/thumbnail is shown
        if (mCurrentPhotoPath != null){
            removeImageDialog();
            return;
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        final CharSequence[] items = { "Take photo", "Choose from gallery", "Cancel"};
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take photo")) {
                EditableReportActivityPermissionsDispatcher.takePhotoWithCheck(this);
            } else if (items[item].equals("Choose from gallery")) {
                EditableReportActivityPermissionsDispatcher.pickPhotoWithCheck(this);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    public void takePhoto() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                photoFile = ImageUtils.createTempImageFile(getApplicationContext());
            } catch (Exception ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.resourceApp.android.fileprovider", photoFile);
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePhotoIntent, 1);
            }
        }
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void pickPhoto(){
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhotoIntent , 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (requestCode == 1){
            mCurrentPhotoPath = photoFile.getAbsolutePath();
        }
        else if (requestCode == 2){
            Uri uri = data.getData();
            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            mCurrentPhotoPath = cursor.getString(columnIndex); // returns null
            cursor.close();
        }
        setThumbnail();
        // Compress image.
        photoFile = ImageUtils.saveScaledPhotoToFile(mCurrentPhotoPath,getApplicationContext());
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public void setThumbnail() {
        // Get the dimensions of the View, Sufficient to check just one dimension
        if(ivThumbnailW == 0){
            ivThumbnailW = nrv.getImageView().getWidth();
            ivThumbnailH = nrv.getImageView().getHeight();
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/ivThumbnailW, photoH/ivThumbnailH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        nrv.getImageView().setImageBitmap(bitmap);
        nrv.getImageView().setAdjustViewBounds(true);
    }

    public void removeImageDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Remove photo?");
        builder.setPositiveButton("Yes", (dialog, id) -> {
            ImageView cameraImageView = nrv.getImageView();
            cameraImageView.setImageDrawable(null);
            cameraImageView.setAdjustViewBounds(false);
            cameraImageView.setImageResource(R.drawable.ic_add_photo);
            mCurrentPhotoPath = null;
        });
        builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void submitReport(){
        // Validates form input
        ArrayList<ReportField> formInput = processWidgetInput(true);

        // if the form input passes validation checks, submit report
        if (formInput != null)
            sendData(formInput, true);
    }

    // Retrieves report input
    // Validates input if report is being submitted
    public ArrayList<ReportField> processWidgetInput(boolean reportSubmission){
        int errorCount = 0;

        EditText subjectEt = nrv.getSubjectValue();
        String subject = subjectEt.getText().toString();
        if(subject.matches("")) {
            errorCount++;
            subjectEt.setError("Subject cannot be empty");
        }

        ArrayList<ReportField> formInput = new ArrayList<>();
        ArrayList<View> inputWidgets = nrv.getInputWidgets();
        int widgetNo = inputWidgets.size();

        // Iterates through all the widgets and stores the inputs in formInput.
        // inputWidgets are defined in EditableReportUIConfig
        for( int i = 0; i < widgetNo; i++ ) {
            View widget = inputWidgets.get(i);
            // getClass().getName() returns android.widget.TextView so we remove "android.widget."
            String widgetName = widget.getClass().getName().substring(15);
            String fieldTitle = widget.getTag().toString();
            String fieldValue = "";
            switch(widgetName){

                case "EditText" :
                    // Also takes input for DatePickerDialog which is represented in an EditTextView
                    EditText et = (EditText) widget;
                    fieldValue = et.getText().toString();
                    // User will only have to enter every field if they want to submit a report
                    // For drafts, fields can be left empty
                    if(fieldValue.matches("") && reportSubmission == true) {
                        errorCount++;
                        // EditText field pops up with error
                        et.setError(fieldTitle + " cannot be empty");
                    }
                    formInput.add(new ReportField(fieldTitle, fieldValue));
                    break;

                case "Spinner" :
                    Spinner sp = (Spinner) widget;
                    fieldValue = sp.getSelectedItem().toString();
                    formInput.add(new ReportField(fieldTitle, fieldValue));
                    break;

                case "Switch" : {
                    // Read EditableReportUIConfig
                    Switch sv = (Switch) widget;
                    SubFormField field = (SubFormField) sv.getTag();
                    fieldTitle = field.getFieldTitle();
                    if (sv.isChecked())
                        fieldValue = field.getOptions().get(0); // first index refers to positive result.
                    else
                        fieldValue = field.getOptions().get(1);
                    formInput.add(new ReportField(fieldTitle, fieldValue));
                    break;
                }
                case "RadioGroup" : {
                    RadioGroup rg = (RadioGroup) widget;
                    SubFormField field = (SubFormField) rg.getTag();
                    fieldTitle = field.getFieldTitle();
                    int checkedRbId = rg.getCheckedRadioButtonId();
                    if(checkedRbId == -1)
                        errorCount++;
                    else {
                        fieldValue = ((RadioButton) findViewById(checkedRbId)).getText().toString();
                    }
                    formInput.add(new ReportField(fieldTitle, fieldValue));
                    break;
                }
                case "ImageView" : {
                    if (mCurrentPhotoPath != null)
                        formInput.add(new ReportField(fieldTitle, Integer.toString(1)));
                    else
                        formInput.add(new ReportField(fieldTitle, Integer.toString(-1)));
                    break;
                }
                default:
                    break;
            }
        }
        if(reportSubmission == true && errorCount != 0) {
            formInput = null;
            Utils.showShortToastMessage(this, "Please fill in all fields.");
        }
        return formInput;
    }

    public Report generateReportObject(ArrayList<ReportField> formInput){
        Report report = new Report();
        // report 'sub category' set to title of sub form.
        report.setSubId(subForm.getSubId());
        report.setSubTitle(subForm.getCategoryTitle());
        SharedPreferences sp = Utils.getSharedPrefs(this);
        report.setUserId(sp.getString(Constants.USER_ID,""));
        report.setUserEmail(sp.getString(Constants.EMAIL,""));
        report.setCompanyId(sp.getString(Constants.COMPANY_ID,""));
        report.setDateModified(Utils.getCurrentDate());
        String refNo = getReportRefNo();
        report.setRefNo(refNo);
        // Undecided as to what to have as the title
        report.setSubjectTitle(nrv.getSubjectValue().getText().toString());
        // Set status as open, which is represented by the value 1.
        report.setStatus(1);

        ArrayList<ReportSection> reportSections = new ArrayList<>();

        ArrayList<SubFormSection> sections = subForm.getSections();
        int sectionsNo = sections.size();

        // Index must be equal in report fields and subform fields.
        // Make sure every input widget's input is added as a Report object to formInput in processWidgetInput()
        // Make sure the same widget cases are accounted for in processWidgetInput() and initView().
        for (int index = 0, i = 0; i < sectionsNo; i++) {
            ArrayList<ReportField> reportFields = new ArrayList<>();

            SubFormSection subFormSection = sections.get(i);
            ArrayList<SubFormField> fields = subFormSection.getSectionFields();
            int fieldsNo = fields.size();
            for (int j = 0; j < fieldsNo; j++) {
                reportFields.add(formInput.get(index++));
            }
            reportSections.add(new ReportSection(subFormSection.getSectionTitle(), reportFields));
        }
        report.setSections(reportSections);
        return report;
    }

    public void sendData(ArrayList<ReportField> formInput, boolean reportSubmission) {
        Report report = generateReportObject(formInput);
        String mEmail = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.EMAIL,"");

        progressDialog = Utils.getProgressDialog(this);

        if(reportSubmission)
            MongoDbConn.getRetrofit().sendReport(mEmail,report)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            response -> handleResponse(reportSubmission),
                            throwable -> handleSubmitReportError(report, throwable)
                    );
        else
            saveDraft(mEmail, report);
    }

    // Creates a new draft entry if draft is made from NewReportAcrivity
    // Or updates existing draft entry if draft is saved from DraftReportActivity
    public abstract void saveDraft(String mEmail, Report report);

    public void handleResponse(boolean reportSubmission) {
        // If submitted from a draft, the draft is deleted.
        if(reportSubmission && draftActivity){
            ArrayList<String> draftId = new ArrayList<>();
            draftId.add(reportRefNo);
            // The second parameter MUST be 'false' otherwise any images will be deleted..
            SyncUtils.deleteDrafts(draftId, false, this, getBaseContext());
        }
        // upload image to database if reports contains one
        if(photoFile != null) {
            String fileName = ImageUtils.getS3ImagePath(reportRefNo, this);
            S3Conn conn = new S3Conn(this.getApplicationContext(), this, getBaseContext(), 1);
            conn.uploadImageFile(photoFile, fileName, reportSubmission, progressDialog); // Async task
        }
        else{ // no photo in draft/report submission.
            // syncing sent reports/drafts so they appear after clearing the activity.
            if(reportSubmission) {
                SyncUtils.syncSentReports(this, getBaseContext(), progressDialog);
                Utils.showLongToastMessage(getBaseContext(), "Report submitted.");
            }
            if(!reportSubmission || (reportSubmission && draftActivity)) {
                SyncUtils.syncUserProfile(this, getBaseContext(), progressDialog);
                if(!reportSubmission)
                    Utils.showLongToastMessage(getBaseContext(), "Draft saved.");
            }
        }

        // Sets the appropriate fragment to be opened when opening NavigationDrawerActivity
        SharedPreferences.Editor editor = Utils.getSharedPrefs(this).edit();
        int fragId;
        if (reportSubmission)
            fragId = R.id.nav_sent_reports;
        else
            fragId = R.id.nav_drafts;
        editor.putInt(PREV_VIEWED_MENU_ITEM_ID, fragId);
        editor.apply();
    }

    public void handleSubmitReportError(Report report, Throwable error) {
        progressDialog.dismiss();
        Utils.showLongToastMessage(getBaseContext(), "Report failed to send. " +
                "Check your internet connection.");
        // Dialog to put report in Outbox
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("You currently have no internet connection.");
        builder.setMessage("Send report to outbox?");
        builder.setPositiveButton("Yes", (dialog, id) -> {
//            if(draftActivity)
//                deleteOutboxReportFromDrafts(report);
            addOffline(report, Constants.OUTBOX_REPORTS_ARRAY, R.id.nav_outbox);
        });
        builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void handleSaveDraftError(Report report, Throwable error) {
        progressDialog.dismiss();
        Utils.showLongToastMessage(getBaseContext(), "Draft failed to save. Please check your internet connection.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("You currently have no internet connection.");
        builder.setMessage("Would you like to save this draft offline?");
        builder.setPositiveButton("Yes", (dialog, id) ->
                addOffline(report, Constants.DRAFT_OFFLINE_REPORTS_ARRAY, R.id.nav_drafts_offline));
        builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void addOffline(Report report, String sharedPrefKey, int prevMenuItemId){
        SharedPreferences sp = Utils.getSharedPrefs(this);
        String reportsJson = sp.getString(sharedPrefKey, "");
        ArrayList<Report> reportsList;
        if(reportsJson != "")
            reportsList = new Gson().fromJson(reportsJson,
                    new TypeToken<ArrayList<Report>>(){}.getType());
        else
            reportsList = new ArrayList<>();
        // Avoids having reports with the same reference no submitted.
        reportRefNo = generateRefNo();
        report.setRefNo(reportRefNo);
        reportsList.add(0,report);
        // Save photo to internal memory.
        if(photoFile != null) {
            try {
                String s3ImagePath = ImageUtils.getS3ImagePath(reportRefNo, this);
                File imageFile = ImageUtils.createImageFile(s3ImagePath, getApplicationContext());
                // Copying temp file to memory.
                FileUtils.copyFile(photoFile, imageFile);
            } catch (Exception e) {
            }
        }
        reportsJson = new Gson().toJson(reportsList);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(sharedPrefKey, reportsJson);
        editor.putInt(PREV_VIEWED_MENU_ITEM_ID, prevMenuItemId);
        editor.apply();
        Utils.clearActivityStack(this);
    }

    public String getReportRefNo(){
        // Divided by 100 so it is the current millis to the nearest tenth of a second.
        // 01/01/2016 millis subtracted to make the number smaller.
        if(reportRefNo != null)
            return reportRefNo;
        else
            reportRefNo = generateRefNo();
        return reportRefNo;
    }

    public String generateRefNo(){
        Long sinceEpoch = System.currentTimeMillis()/100;
        Long since2016 = 14516064000L;
        Long now = sinceEpoch - since2016;
        char randomAlphaNumChar = (char) (65 + new Random().nextInt(26));
        return Character.toString(randomAlphaNumChar) + Long.toString(now);
    }

    public void initBundle(Bundle savedInstanceState){
        Bundle bundle = getIntent().getBundleExtra("bundle");
        if (savedInstanceState != null) {
            savedState = savedInstanceState.getBundle(Constants.REPORT_TEMPLATE_BUNDLE);
            subForm = savedState.getParcelable(Constants.REPORT_TEMPLATE_BUNDLE);
            mCurrentPhotoPath = savedState.getString(IMAGE_VIEW_PATH_TAG);
            ivThumbnailW = savedState.getInt(IMAGE_VIEW_WIDTH);
            ivThumbnailH = savedState.getInt(IMAGE_VIEW_HEIGHT);
        }else
            savedState = bundle;
        if(savedState != null) {
            subForm = savedState.getParcelable(Constants.REPORT_TEMPLATE_BUNDLE);
            mCurrentPhotoPath = savedState.getString(IMAGE_VIEW_PATH_TAG);
            ivThumbnailW = savedState.getInt(IMAGE_VIEW_WIDTH);
            ivThumbnailH = savedState.getInt(IMAGE_VIEW_HEIGHT);
        }
    }

    public Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelable(Constants.REPORT_TEMPLATE_BUNDLE, subForm);
        state.putString(IMAGE_VIEW_PATH_TAG, mCurrentPhotoPath);
        state.putInt(IMAGE_VIEW_WIDTH, ivThumbnailW);
        state.putInt(IMAGE_VIEW_HEIGHT, ivThumbnailH);
        return state;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (subForm == null) {
            outState.putBundle(Constants.REPORT_TEMPLATE_BUNDLE, savedState);
        } else {
            outState.putBundle(Constants.REPORT_TEMPLATE_BUNDLE, createdStateInDestroyView ? savedState : saveState());
        }
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        EditableReportActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.editable_report_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        // Save draft, boolean "reportSubmission" parameter set to false.
        if (id == R.id.action_save_draft) {
            ArrayList<ReportField> formInput = processWidgetInput(false);
            sendData(formInput, false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You have unsaved changes.\nDo you want to discard them?");
        builder.setPositiveButton("Discard", (dialog, id) -> {
            finish(); });
        builder.setNegativeButton("Cancel", (dialog, id) -> {
            dialog.cancel(); });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void initView(){
        nrv = new EditableReportUIConfig(activityContext);

        /* Draft case only. */
        Report draft = null;
        ArrayList<ReportSection> reportSections = null;
        String subjectTitle = "";
        if(draftActivity) {
            draft = getIntent().getBundleExtra("bundle")
                    .getParcelable(Constants.REPORT_INPUT_BUNDLE);
            // Saving draft again will overwrite the same one instead of creating a duplicate
            reportRefNo = draft.getRefNo();
            // Prepopulate subject header field.
            subjectTitle = draft.getSubjectTitle();
            reportSections = draft.getSections();
        }
        /*------------------*/
        nrv.addSubjectEditTv(subjectTitle);

        ArrayList<SubFormSection> sections = subForm.getSections();
        int sectionsNo = sections.size();

        for (int i = 0; i < sectionsNo; i++) {
            /* Draft case only. */
            ReportSection reportSection = null;
            ArrayList<ReportField> reportFields = null;
            if(draftActivity) {
                reportSection = reportSections.get(i);
                reportFields = reportSection.getSectionFields();
            }
            /*------------------*/

            SubFormSection section = sections.get(i);
            nrv.addSectionHeaderTv(section.getSectionTitle());

            ArrayList<SubFormField> fields = section.getSectionFields();
            int fieldsNo = fields.size();

            for (int j = 0; j < fieldsNo; j++) {
                /* Draft case only. */
                String savedValue = "";
                if(draftActivity)
                    savedValue = reportFields.get(j).getValue();
                /*------------------*/

                SubFormField field = fields.get(j);
                String widget = field.getWidget();
                // Unique id for each element, ensures that on rotation of screen, state is saved.
                int id = Integer.parseInt(Integer.toString(i) + Integer.toString(j));

                nrv.addBlankView(5);
                // Note: ImageView is a separate case and is dealt with in
                // NewReportActivity and DraftReportActivity externally
                switch (widget) {
                    case "EditText":
                        nrv.addEditTextView(field, id, savedValue);
                        break;
                    case "EditTextBox":
                        nrv.addEditTextBoxView(field, id, savedValue);
                        break;
                    case "Spinner":
                        nrv.addSpinnerView(field, id, savedValue);
                        break;
                    case "Switch":
                        nrv.addSwitchView(field, id, savedValue);
                        break;
                    case "RadioGroup":
                        nrv.addRadioGroupView(field, id, savedValue);
                        break;
                    case "DatePicker":
                        nrv.addDatePickerDialog(field, id, savedValue, activityContext);
                        break;
                    case "Currency" :
                        nrv.addCurrencyView(field, id, savedValue);
                        break;
                    case "CheckBox":
                        break;
                    case "ImageView": {
                        addImageViewCase(field, id, savedValue);
                        break;
                    }
                }
            }
            if (i < sectionsNo - 1)
                nrv.addSectionBreak();
        }

        nrv.addSubmitButton();

        ScrollView scrollView = nrv.getScrollView();
        LinearLayout view = (LinearLayout)findViewById(R.id.activity_editable_report);
        view.addView(scrollView);

        nrv.getSubmitBtn().setOnClickListener(v -> onSubmitPressedDialog());
    }

    public abstract void addImageViewCase(SubFormField field, int id, String savedValue);

}