package com.resourceApp.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.resourceApp.model.Response;
import com.resourceApp.utils.ImageUtils;
import com.resourceApp.utils.SyncUtils;
import com.resourceApp.utils.Utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import rx.Observable;

import static com.resourceApp.utils.Utils.allowOrientationChange;
import static com.resourceApp.utils.Utils.preventOrientationChange;

/**
 * Created by Osman on 17/01/2017.
 */

public class S3Conn {

    private AmazonS3 s3Client;
    private Regions region = Regions.US_EAST_1;
    String identityPoolId = "us-east-1:56be34f0-3016-4040-a2cb-cd4f6d470a44";
    private String bucket;
    private String sentReportImagesBucket = "r-sent-report-images";
    private String companyLogoBucket = "r-company-logos";
    private String fileName;
    private String reportRef;
    private File fileToUpload;
    private ImageView imageView;
    private Context applicationContext;
    private Context activityContext;
    private Context baseContext;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;

    public S3Conn(Context applicationContext, Context activityContext, Context baseContext,
                  Integer bucket){
        this.applicationContext = applicationContext;
        this.activityContext = activityContext;
        this.baseContext = baseContext;
        switch(bucket) {
            case 2:
                this.bucket = companyLogoBucket;
                break;
            case 1:
            default:
                this.bucket = sentReportImagesBucket;
                break;
        }
        credentialsProvider();
    }

    public void credentialsProvider(){
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                applicationContext, identityPoolId, region
        );
        s3Client = new AmazonS3Client(credentialsProvider);
    }

    public void uploadImageFileBroadcast(File fileToUpload, String fileName, String reportRef){
        this.fileName = fileName;
        this.fileToUpload = fileToUpload;
        this.reportRef = reportRef;
        executeImageUploadBroadcast asyncTask = new executeImageUploadBroadcast();
        asyncTask.execute();
    }

    private class executeImageUploadBroadcast extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try{
                PutObjectRequest putRequest = new PutObjectRequest(
                        bucket, fileName, fileToUpload
                );
                PutObjectResult putResponse = s3Client.putObject(putRequest);
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void v) {
            // update preferences.
            SharedPreferences sp = Utils.getSharedPrefs(activityContext);
            SyncUtils.removeReportFromOutbox(reportRef,sp);
        }
    }

    public void uploadImageFile(File fileToUpload, String fileName, Boolean reportSubmission, ProgressDialog progressDialog){
        this.fileName = fileName;
        this.fileToUpload = fileToUpload;
        this.progressDialog = progressDialog;
        executeImageUpload asyncTask = new executeImageUpload();
        asyncTask.execute(reportSubmission);
    }

    private class executeImageUpload extends AsyncTask<Boolean,Void,Boolean> {
        @Override
        protected Boolean doInBackground(Boolean... params) {
            try{
                PutObjectRequest putRequest = new PutObjectRequest(
                        bucket, fileName, fileToUpload
                );
                PutObjectResult putResponse = s3Client.putObject(putRequest);
            } catch(Exception e){
                e.printStackTrace();
            }

            return params[0];
        }

        protected void onPostExecute(Boolean reportSubmission) {
            if(reportSubmission)
                SyncUtils.syncSentReports(activityContext, baseContext, progressDialog);
            else
                SyncUtils.syncUserProfile(activityContext, baseContext, progressDialog);
        }
    }

    public void downloadImageFile(String s3ImagePath, ImageView imageView, ProgressBar progressBar){
        this.imageView = imageView;
        this.progressBar = progressBar;
        progressBar.setVisibility(View.VISIBLE);
        imageView.setImageBitmap(null);
        executeImageDownload asyncTask = new executeImageDownload();
        asyncTask.execute(s3ImagePath);
    }

    private class executeImageDownload extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... s3ImagePath) {
            String imagePath = null;
            try {
                GetObjectRequest getRequest = new GetObjectRequest(bucket, s3ImagePath[0]);
                S3Object getResponse = s3Client.getObject(getRequest);

                if(getResponse != null){
                    InputStream objectContent = getResponse.getObjectContent();
                    // create image file
                    File imageFile = ImageUtils.createImageFile(s3ImagePath[0], applicationContext);
                    // image view path overwritten with image file path
                    imagePath = imageFile.getAbsolutePath();
                    // image file actually made
                    IOUtils.copy(objectContent, new FileOutputStream(imageFile));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return imagePath;
        }

        protected void onPostExecute(String imagePath) {
            if(imagePath != null) {
                ImageUtils.setImageViewBitmap(imageView, imagePath);
            }
            progressBar.setVisibility(View.INVISIBLE);
            allowOrientationChange(activityContext);
        }
    }
}
