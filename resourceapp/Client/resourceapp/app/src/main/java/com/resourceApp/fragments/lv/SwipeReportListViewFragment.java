package com.resourceApp.fragments.lv;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.model.Report;
import com.resourceApp.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

import static com.resourceApp.utils.Constants.PREV_VIEWED_MENU_ITEM_ID;
import static com.resourceApp.utils.Utils.showLongToastMessage;

public abstract class SwipeReportListViewFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    protected ListView listView;
    protected int layout_id;
    protected int listView_id;
    protected int swipeLayout_id;
    protected int placeholder_id;
    protected SharedPreferences sp;
    protected String constantsKey;
    protected String json;
    protected ArrayList<Report> reports;
    protected ArrayAdapter<Report> adapter;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected View view;
    protected Dialog overlayDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initViewParams();
        view = inflater.inflate(layout_id, container, false);
        postInitViewParams();
        initView();
        initSwipe();
        return view;
    }

    // layout_id, constantsKey, listView_id, swipeLayout_id, viewTitle title MUST ALL be set.
    public abstract void initViewParams();

    // Optional to override
    public void postInitViewParams() {}

    private void initView(){
        sp = Utils.getSharedPrefs(getActivity());
        json = sp.getString(constantsKey,"");
        // Often returns a string of length 2 : '[]'
        if(json.length() > 2)
            populateList();
        else if(placeholder_id != 0) {
            view.findViewById(listView_id).setVisibility(View.GONE);
            view.findViewById(placeholder_id).setVisibility(View.VISIBLE);
        }
    }

    public void populateList() {
        reports = new Gson().fromJson(json, new TypeToken<ArrayList<Report>>(){}.getType());
        initAdapter();
        listView = (ListView) view.findViewById(listView_id);
        if(placeholder_id != 0)
            view.findViewById(placeholder_id).setVisibility(View.GONE);
        listView.setAdapter(adapter);
        listView.setVisibility(View.VISIBLE);
        listView.setOnItemClickListener((arg0, view1, pos, arg3) -> onItemClick(pos));
        listViewOptionalInit();
    }

    public abstract void initAdapter();

    // Optional to override
    public void listViewOptionalInit(){}

    // OnItemClickListener
    public abstract void onItemClick(int pos);

    private void initSwipe(){
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(swipeLayout_id);
        swipeRefreshLayout.setOnRefreshListener(() -> onRefresh());
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        overlayDialog = Utils.getOverlayDialog(getActivity());
        refreshRequest();
    }

    // Must contain success or afterRefresh
    // Otherwise resource leakage exception will be thrown.
    public abstract void refreshRequest();

    public void success(){
        afterRefresh();
        initView();
    }

    public void error(){
        showLongToastMessage(getActivity(), "Request error");
        afterRefresh();
    }

    private void afterRefresh(){
        swipeRefreshLayout.setRefreshing(false);
        overlayDialog.dismiss();
    }

    // Only applies to offline drafts and outbox.
    public void deleteOfflineReports(String sharedPrefKey, int prevMenuItemId, String toastMessageReportType){
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        Log.d("checked", checked.toString());

        ArrayList<Integer> deleteIndexes = new ArrayList<>();
        for (int i = 0; i < listView.getAdapter().getCount(); i++) {
            if (checked.get(i))
                deleteIndexes.add(i);
        }
        int deleteCount = deleteIndexes.size();
        Collections.sort(deleteIndexes,Collections.reverseOrder());

        for(int i : deleteIndexes)
            reports.remove(i);

        String offlineReportsJson = new Gson().toJson(reports);
        SharedPreferences.Editor editor = Utils.getSharedPrefs(getActivity()).edit();
        editor.putString(sharedPrefKey, offlineReportsJson);
        editor.putInt(PREV_VIEWED_MENU_ITEM_ID, prevMenuItemId);
        editor.apply();
        Utils.showLongToastMessage(getActivity(), deleteCount+" report(s) deleted from " + toastMessageReportType + ".");
        Utils.clearActivityStack(getActivity());
    }

    @Override
    public void onResume(){
        super.onResume();
        initView();
    }

}