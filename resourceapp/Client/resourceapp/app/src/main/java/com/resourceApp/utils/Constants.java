package com.resourceApp.utils;

public class Constants {

    public static final String BASE_URL = "http://34.197.178.132/api/v1/";
    // aws elastic beanstalk - http://34.197.178.132/api/v1/
    // localhost - http://192.168.1.13:8080/api/v1/
    // mobile localhost - http://192.168.42.56:8080/api/v1/
    public static final String TOKEN = "token";
    public static final String EMAIL = "email";
    public static final String USER_ID = "user_id";
    public static final String USER_LEVEL = "user_level";
    public static final String FULL_NAME = "full_name";
    public static final String COMPANY_ID = "company_id";
    public static final String COMPANY_THUMBNAIL = "company_thumbnail";
    // For offline use. Practical for areas in which there is no internet connection i.e factories.
    public static final String SENT_REPORTS_ARRAY = "sent_reports";
    public static final String NEW_REPORT_ARRAY = "new_report_categories";
    public static final String DRAFT_REPORTS_ARRAY = "draft_reports";
    public static final String DRAFT_OFFLINE_REPORTS_ARRAY = "draft_offline_reports";
    public static final String OUTBOX_REPORTS_ARRAY = "outbox_reports";
    public static final String ADMIN_OPEN_REPORTS = "admin_open_reports";
    public static final String ADMIN_CLOSED_REPORTS = "admin_closed_reports";

    public static final String REPORT_INPUT_BUNDLE = "report_input";
    public static final String REPORT_TEMPLATE_BUNDLE = "report_template";
    public static final String REPORT_STATUS_UPDATES = "report_status_updates";
    public static final String PROFILE_SET = "profile_set";
    public static final String SYNC_PENDING = "sync_confirm";
    public static final String ADMIN_SYNC_PENDING = "admin_sync";
    public static final String PREV_VIEWED_MENU_ITEM_ID = "prev_menu_itemId";
}
