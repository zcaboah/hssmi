package com.resourceApp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.view.Surface;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.resourceApp.NavigationDrawerActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Osman on 19/01/2017.
 */

public class Utils {

    public static void showShortToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void showLongToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static Dialog getOverlayDialog(Context context){
        Dialog mOverlayDialog = new Dialog(context, android.R.style.Theme_Panel);
        mOverlayDialog.setCancelable(false);
        mOverlayDialog.show();
        return mOverlayDialog;
        // call .dismiss() on the returned object to dismiss dialog.
    }

    public static ProgressDialog getProgressDialog(Context context){
        ProgressDialog progressDialog = ProgressDialog.show(context,null,null);
        progressDialog.setContentView(new ProgressBar(context));
        return progressDialog;
    }

    public static ProgressBar getProgressBar(Context context){
        preventOrientationChange(context);
        ProgressBar progressBar = new ProgressBar(context);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.INVISIBLE);
        return progressBar;
    }

    public static void clearActivityStack(Context activityContext){
        Intent i = new Intent(activityContext, NavigationDrawerActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        activityContext.startActivity(i);
    }

    public static SharedPreferences getSharedPrefs(Context activityContext){
        return PreferenceManager.getDefaultSharedPreferences(activityContext);
    }

    public static void dismissDialogAndClearStack(ProgressDialog progressDialog, Context activityContext){
        progressDialog.dismiss();
        ((Activity)activityContext).
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        Utils.clearActivityStack(activityContext);
    }

    public static void preventOrientationChange(Context activityContext){
        Activity activity = ((Activity)activityContext);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        if (rotation == Surface.ROTATION_0)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (rotation == Surface.ROTATION_90)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (rotation == Surface.ROTATION_270)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    }

    public static void allowOrientationChange(Context activityContext){
        Activity activity = ((Activity)activityContext);
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        return sdf.format(new Date()).toString();
    }

}
