package com.resourceApp;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.resourceApp.model.Report;
import com.resourceApp.model.StatusUpdate;
import com.resourceApp.model.SubForm;
import com.resourceApp.utils.Constants;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;

public class StatusUpdatesActivity extends AppCompatActivity {

    ArrayList<StatusUpdate> statusUpdates;
    Bundle savedState = null;
    boolean createdStateInDestroyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_updates);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable
                (getResources().getColor(R.color.colorPrimaryDark)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("ResourceApp");
        initBundle(savedInstanceState);
        initView();
    }

    private void initView() {
        if(statusUpdates.size() == 0){
            findViewById(R.id.statusUpdatesPlaceholder).setVisibility(View.VISIBLE);
            return;
        }
        // Reverse ArrayList for reverse chronological order of status updates.
        Collections.reverse(statusUpdates);
        StatusUpdateArrayAdapter adapter = new StatusUpdateArrayAdapter(this, statusUpdates);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.lvStatusUpdates);
        listView.setAdapter(adapter);
        View header = View.inflate(this, R.layout.tv_status_updates_header, null);
        listView.addHeaderView(header);
    }

    public class StatusUpdateArrayAdapter extends ArrayAdapter<StatusUpdate> {
        public StatusUpdateArrayAdapter(Context context, ArrayList<StatusUpdate> statusUpdates) {
            super(context, 0, statusUpdates);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            StatusUpdate statusUpdate = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).
                        inflate(R.layout.item_listview_status_updates, parent, false);
            }
            TextView tvStatusValue = (TextView) convertView.findViewById(R.id.tvStatusValue);
            TextView tvDate = (TextView) convertView.findViewById(R.id.tvStatusDate);
            TextView tvEmail = (TextView) convertView.findViewById(R.id.tvEmailValue);
            TextView tvComment = (TextView) convertView.findViewById(R.id.tvCommentValue);
            // Status is set to int so the corresponding String value is in report.
            // I did this to save marginal memory, cba to change it back.
            Report report = new Report();
            report.setStatus(statusUpdate.getStatus());

            tvStatusValue.setText(report.getStatus());
            tvDate.setText(statusUpdate.getDate());
            tvEmail.setText(statusUpdate.getAdminEmail());

            String comment = statusUpdate.getComment();
            if (comment.matches(""))
                tvComment.setText("Not provided");
            else
                tvComment.setText(statusUpdate.getComment());

            ImageView iv = (ImageView) convertView.findViewById(R.id.ivStatusIcon);
            if (statusUpdate.getStatus() == 1)
                iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_admin_open_report));
            else
                iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_admin_close_report));

            return convertView;
        }
    }

    private void initBundle(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getBundleExtra("bundle");
        if (savedInstanceState != null) {
            savedState = savedInstanceState.getBundle(Constants.REPORT_STATUS_UPDATES);
            statusUpdates = savedState.getParcelableArrayList(Constants.REPORT_STATUS_UPDATES);
        } else
            savedState = bundle;
        if (savedState != null)
            statusUpdates = savedState.getParcelableArrayList(Constants.REPORT_STATUS_UPDATES);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelableArrayList(Constants.REPORT_STATUS_UPDATES, statusUpdates);
        return state;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (statusUpdates == null)
            outState.putBundle(Constants.REPORT_STATUS_UPDATES, savedState);
        else
            outState.putBundle(Constants.REPORT_STATUS_UPDATES, createdStateInDestroyView ? savedState : saveState());
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
