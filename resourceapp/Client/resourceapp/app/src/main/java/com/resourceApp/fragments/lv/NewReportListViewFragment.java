package com.resourceApp.fragments.lv;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.R;
import com.resourceApp.model.SubForm;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.NewReportActivity;
import com.resourceApp.fragments.lv.lv_array_adapters.NewReportArrayAdapter;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.SyncUtils;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NewReportListViewFragment extends SwipeReportListViewFragment {

    /*
    Since we do not deal with Report objects here but rather SubForm objects,
    the initAdapter() method cannot be used and the populateList() method
    MUST be overridden as a result.
     */

    private ArrayList<SubForm> subForms;

    @Override
    public void initViewParams() {
        constantsKey = Constants.NEW_REPORT_ARRAY;
        layout_id = R.layout.fragment_new_report;
        listView_id = R.id.lvSubs;
        swipeLayout_id = R.id.swipe_new_report_list;
        getActivity().setTitle("New report");
    }

    @Override
    public void initAdapter() {
        // Does not apply here as it is not a report.
    }

    @Override
    public void populateList() {
        subForms = new Gson().fromJson(json, new TypeToken<ArrayList<SubForm>>(){}.getType());
        NewReportArrayAdapter adapter = new NewReportArrayAdapter(view.getContext(), subForms);
        // Attach the adapter to a ListView
        listView = (ListView) view.findViewById(listView_id);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((arg0, view1, pos, arg3) -> onItemClick(pos));
    }

    @Override
    public void onItemClick(int pos) {
        SubForm subForm = subForms.get(pos);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.REPORT_TEMPLATE_BUNDLE, subForm);

        Intent intent = new Intent(getActivity(), NewReportActivity.class);
        intent.putExtra("bundle", bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void refreshRequest() {
        String mCompanyId = sp.getString(Constants.COMPANY_ID, "");
        MongoDbConn.getRetrofit().getCompanyProfile(mCompanyId)
                .doOnNext(company -> SyncUtils.syncCompanySharedPrefs(company,sp))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> success(), err -> error());
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
