package com.resourceApp.admin;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.resourceApp.R;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.SyncUtils;
import com.resourceApp.utils.Utils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AdminFragment extends Fragment {

    SharedPreferences sp;
    String companyId;
    ViewPager viewPager;
    AdminPagerAdapter adapter;
    boolean syncPending;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPrefs();
    }

    public void initPrefs(){
        sp = Utils.getSharedPrefs(getActivity());
        companyId = sp.getString(Constants.COMPANY_ID,"");
        String openReports = sp.getString(Constants.ADMIN_OPEN_REPORTS, null);
        String closedReports = sp.getString(Constants.ADMIN_CLOSED_REPORTS, null);
        syncPending = sp.getBoolean(Constants.ADMIN_SYNC_PENDING, false);

        if (syncPending || (openReports == null && closedReports == null)) {
            syncAdminReports();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Admin");
        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.admin_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Open reports"));
        tabLayout.addTab(tabLayout.newTab().setText("Closed reports"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.admin_pager);
        adapter = new AdminPagerAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return view;
    }

     public void syncAdminReports(){
         ProgressDialog progressDialog = Utils.getProgressDialog(getActivity());
         MongoDbConn.getRetrofit().getOpenReports(companyId)
                .doOnNext(reports -> SyncUtils.syncOpenReportsSharedPrefs(reports, sp))
                .concatMap(r -> MongoDbConn.getRetrofit().getClosedReports(companyId))
                .doOnNext(reports -> SyncUtils.syncClosedReportsSharedPrefs(reports,sp))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        r -> requestComplete(progressDialog),
                        error -> requestComplete(progressDialog));
    }

    public void requestComplete(ProgressDialog progressDialog){
        progressDialog.dismiss();
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Constants.ADMIN_SYNC_PENDING, false);
        editor.apply();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume(){
        super.onResume();
        initPrefs();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}