package com.resourceApp.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.resourceApp.R;
import com.resourceApp.fragments.lv.lv_array_adapters.ReportArrayAdapter;
import com.resourceApp.fragments.lv.SwipeReportListViewFragment;
import com.resourceApp.model.Report;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.SyncUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OpenReportListViewFragment extends SwipeReportListViewFragment {

    @Override
    public void initViewParams() {
        // layout_fragment_id, constantsKey, listView_id, swipeLayout_id, viewTitle.
        layout_id = R.layout.fragment_admin_open_reports;
        constantsKey = Constants.ADMIN_OPEN_REPORTS;
        listView_id = R.id.lvOpenReports;
        swipeLayout_id = R.id.swipe_admin_open;
        placeholder_id = R.id.openReportsPlaceholder;
        getActivity().setTitle("Admin");
    }

    @Override
    public void initAdapter() {
        adapter = new ReportArrayAdapter(view.getContext(), reports,
                R.layout.item_listview_admin_open_report);
    }

    @Override
    public void listViewOptionalInit() {
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new modeCallback());
    }

    @Override
    public void onItemClick(int pos){
        // Parceling Report object to send to SentReportActivity
        Report report = reports.get(pos);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.REPORT_INPUT_BUNDLE, report);
        Intent intent = new Intent(getActivity(), AdminReportActivity.class);
        intent.putExtra("bundle", bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void refreshRequest() {
        String companyId = sp.getString(Constants.COMPANY_ID, "");
        MongoDbConn.getRetrofit().getOpenReports(companyId)
                .doOnNext(reports -> SyncUtils.syncOpenReportsSharedPrefs(reports, sp))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> success(), err -> error());
    }

    // optional implementation.
    private class modeCallback implements ListView.MultiChoiceModeListener {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int pos, long id, boolean check) {
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.lv_selection_menu, menu); // change menu
            menu.findItem(R.id.close_report).setVisible(true);
            actionMode.setTitle("Select reports");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            AlertDialog alert;
            switch (menuItem.getItemId()) {
                case R.id.delete_report:
                    builder.setMessage("Are you sure you want to delete the selected reports?");
                    builder.setPositiveButton("Yes", (dialog, id) -> {
                        dialog.dismiss();
                    });
                    builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
                    alert = builder.create();
                    alert.show();
                    break;
                case R.id.close_report:
                    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Are you sure you want to close the selected reports?");
                    builder.setPositiveButton("Yes", (dialog, id) -> {
                        dialog.dismiss();
                    });
                    builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
                    alert = builder.create();
                    alert.show();
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
        }
    }
}