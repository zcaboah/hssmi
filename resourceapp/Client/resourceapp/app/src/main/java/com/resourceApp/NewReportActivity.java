package com.resourceApp;

import android.os.Bundle;

import com.resourceApp.EditableReportActivity;
import com.resourceApp.model.Report;
import com.resourceApp.model.SubFormField;
import com.resourceApp.network.MongoDbConn;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Osman on 28/01/2017.
 */

public class NewReportActivity extends EditableReportActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initViewParams() {
        activityContext = this;
        draftActivity = false;
    }

    @Override
    public void addImageViewCase(SubFormField field, int id, String savedValue){
        nrv.addImageView(field, id);
        nrv.getImageView().setOnClickListener(v -> photoOptionsDialog());
    }

    // Adds new draft entry
    @Override
    public void saveDraft(String mEmail, Report report) {
        MongoDbConn.getRetrofit().saveDraft(mEmail,report)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> handleResponse(false),
                           err -> handleSaveDraftError(report, err));
    }
}
