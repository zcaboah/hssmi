package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Osman on 29/11/2016.
 */

public class ReportSection implements Parcelable {

    private String sectionTitle;
    private ArrayList<ReportField> sectionFields;

    public ReportSection(String sectionTitle, ArrayList<ReportField> sectionFields) {
        this.sectionTitle = sectionTitle;
        this.sectionFields = sectionFields;
    };

    public String getSectionTitle() {
        return sectionTitle;
    }

    public ArrayList<ReportField> getSectionFields() {
        return sectionFields;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sectionTitle);
        dest.writeTypedList(this.sectionFields);
    }

    public ReportSection() {
    }

    public ReportSection(Parcel in) {
        this.sectionTitle = in.readString();
        this.sectionFields = in.createTypedArrayList(ReportField.CREATOR);
    }

    public static final Creator<ReportSection> CREATOR = new Creator<ReportSection>() {
        @Override
        public ReportSection createFromParcel(Parcel source) {
            return new ReportSection(source);
        }

        @Override
        public ReportSection[] newArray(int size) {
            return new ReportSection[size];
        }
    };
}
