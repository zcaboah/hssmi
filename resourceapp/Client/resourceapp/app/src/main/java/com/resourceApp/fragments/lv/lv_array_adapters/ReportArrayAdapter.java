package com.resourceApp.fragments.lv.lv_array_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.model.Report;

import java.util.ArrayList;

public class ReportArrayAdapter extends ArrayAdapter<Report> {

    int layout_item_listview_id;

    public ReportArrayAdapter(Context context, ArrayList<Report> reports, int layout_item_listview_id) {
        super(context, 0, reports);
        this.layout_item_listview_id = layout_item_listview_id;
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent)  {
         // Get the data item for this position
         Report report = getItem(position);
         // Check if an existing view is being reused, otherwise inflate the view
         if (convertView == null) {
             convertView = LayoutInflater.from(getContext())
                     .inflate(layout_item_listview_id, parent, false);
         }
         // Lookup view for data population
         TextView tvReportCategory = (TextView) convertView.findViewById(R.id.tvCategory);
         TextView tvSubjectTitle = (TextView) convertView.findViewById(R.id.tvReportTitle);
         TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
         TextView tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
         TextView tvEmail = (TextView) convertView.findViewById(R.id.tvEmail);
         TextView tvRef = (TextView) convertView.findViewById(R.id.tvRef);

         // Populate the data into the template view using the data object
         if(tvSubjectTitle != null) {
             String subject = report.getSubjectTitle();
             if(subject.matches(""))
                 tvSubjectTitle.setText("Untitled");
             else
                tvSubjectTitle.setText(report.getSubjectTitle());
         }
         if(tvReportCategory != null)
             tvReportCategory.setText(report.getSubTitle());
         if(tvDate != null)
             tvDate.setText(report.getDateModified());
         if(tvStatus != null)
             tvStatus.setText(report.getStatus());
         if(tvEmail != null)
             tvEmail.setText(report.getUserEmail());
         if(tvRef != null)
             tvRef.setText(report.getRefNo());

         // Return the completed view to render on screen
         return convertView;
    }
}
