package com.resourceApp.fragments.dialog;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.resourceApp.R;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.Utils;


public class SyncDialog extends DialogFragment {


    public SyncDialog() {
        // Empty constructor required for DialogFragment
    }

    public static SyncDialog newInstance(String title) {
        SyncDialog frag = new SyncDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(R.string.alert_dialog_msg);
        alertDialogBuilder.setPositiveButton(R.string.alert_dialog_positive, (dialog, which) -> {

            SharedPreferences mSharedPreferences;
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = mSharedPreferences.edit();

            // By setting to true, syncAll() is called in NavigationDrawerActivity
            editor.putBoolean(Constants.SYNC_PENDING, true);

            FragmentManager fm = getActivity().getSupportFragmentManager();
            String rootMenuItemId = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();

            if(rootMenuItemId != null){
                editor.putInt(Constants.PREV_VIEWED_MENU_ITEM_ID, Integer.parseInt(rootMenuItemId));
            }
            editor.commit();

            Utils.clearActivityStack(getActivity());
        });
        alertDialogBuilder.setNegativeButton(R.string.alert_dialog_negative, (dialog, which) -> dialog.dismiss());

        return alertDialogBuilder.create();
    }

}
