package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Osman on 29/11/2016.
 */

public class SubForm implements Parcelable {

    private String sub_title;
    private String sub_id;
    private ArrayList<SubFormSection> sections;

    public SubForm() { }

    public String getSubId() {
        return sub_id;
    }

    public String getCategoryTitle() {
        return sub_title;
    }

    public ArrayList<SubFormSection> getSections() {
        return sections;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sub_id);
        dest.writeString(this.sub_title);
        dest.writeList(this.sections);
    }

    protected SubForm(Parcel in) {
        this.sub_id = in.readString();
        this.sub_title = in.readString();
        this.sections = new ArrayList<>();
        in.readList(this.sections, SubFormSection.class.getClassLoader());
    }

    public static final Creator<SubForm> CREATOR = new Creator<SubForm>() {
        @Override
        public SubForm createFromParcel(Parcel source) {
            return new SubForm(source);
        }

        @Override
        public SubForm[] newArray(int size) {
            return new SubForm[size];
        }
    };
}
