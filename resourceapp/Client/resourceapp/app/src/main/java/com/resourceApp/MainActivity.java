package com.resourceApp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.resourceApp.fragments.LoginFragment;
import com.resourceApp.fragments.dialog.ResetPasswordDialog;
import com.resourceApp.utils.Constants;

public class MainActivity extends AppCompatActivity implements ResetPasswordDialog.Listener {

    public static final String TAG = MainActivity.class.getSimpleName();
    private LoginFragment mLoginFragment;
    private ResetPasswordDialog mResetPasswordDialog;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        if(!mSharedPreferences.getString(Constants.EMAIL, "").isEmpty()){
            Intent intent = new Intent(MainActivity.this, NavigationDrawerActivity.class);
            MainActivity.this.startActivity(intent);
        }
        else {
            loadFragment();
        }
    }

    private void loadFragment(){
        if (mLoginFragment == null) {
            mLoginFragment = new LoginFragment();
        }
        getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,mLoginFragment,LoginFragment.TAG).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String data = "";
        if(intent.getData() != null) {
            data = intent.getData().getLastPathSegment();
            Log.d(TAG, "onNewIntent: " + data);

            mResetPasswordDialog = (ResetPasswordDialog) getFragmentManager().findFragmentByTag(ResetPasswordDialog.TAG);

            if (mResetPasswordDialog != null)
                mResetPasswordDialog.setToken(data);
        }
    }

    @Override
    public void onPasswordReset(String message) {
        showSnackBarMessage(message);
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.activity_main),message,Snackbar.LENGTH_SHORT).show();

    }
}
