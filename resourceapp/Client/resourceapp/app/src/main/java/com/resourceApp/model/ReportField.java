package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Osman on 21/11/2016.
 */

public class ReportField implements Parcelable {

    private String title;
    private String value;

    public ReportField(String title, String value){
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.value);
    }

    protected ReportField(Parcel in) {
        this.title = in.readString();
        this.value = in.readString();
    }

    public static final Parcelable.Creator<ReportField> CREATOR = new Parcelable.Creator<ReportField>() {
        @Override
        public ReportField createFromParcel(Parcel source) {
            return new ReportField(source);
        }

        @Override
        public ReportField[] newArray(int size) {
            return new ReportField[size];
        }
    };
}
