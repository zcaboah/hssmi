package com.resourceApp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.model.Report;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.network.S3Conn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.ImageUtils;
import com.resourceApp.utils.SyncUtils;
import com.resourceApp.utils.Utils;

import java.io.File;
import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osman on 17/02/2017.
 */

public class NetworkReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                Log.d("Network", "Internet YAY");
                SharedPreferences sp = Utils.getSharedPrefs(context);
                String mEmail = sp.getString(Constants.EMAIL,"");
                String outboxJson = sp.getString(Constants.OUTBOX_REPORTS_ARRAY,"");
                // break statement for now.
                if(outboxJson.length() > 2){
                    ArrayList<Report> reports = new Gson().fromJson(
                            outboxJson,new TypeToken<ArrayList<Report>>(){}.getType());
                    int sizeOfOutbox = reports.size();
                    /*
                    Sending individually because you want the pictures only to be uploaded once you
                    have confirmaticon the report has been sent.
                     */
                    for(int i = 0; i < sizeOfOutbox; i++){
                        Report report = reports.get(i);
                        report.setDateModified(Utils.getCurrentDate());
                        String reportRef = report.getRefNo();
                        String s3ImagePath = ImageUtils.getS3ImagePath(reportRef, context);
                        String internalPath = ImageUtils.getInternalImagePath(s3ImagePath, context);
                        final File imageToUpload;
                        if (new File(internalPath).exists())
                            imageToUpload = new File(internalPath);
                        else
                            imageToUpload = null;

                        MongoDbConn.getRetrofit().sendReport(mEmail,report)
                            .doOnNext(r -> {
                                if(imageToUpload != null) {
                                    S3Conn conn = new S3Conn(context,context,null,1);
                                    conn.uploadImageFileBroadcast(imageToUpload, s3ImagePath, reportRef);
                                }
                                else
                                    SyncUtils.removeReportFromOutbox(reportRef, sp);
                            })
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe();
                    }
                }
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                Log.d("Network", "No internet :(");
            }
        }
    }
}
