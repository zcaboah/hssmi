package com.resourceApp.activity_ui_config;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Osman on 14/02/2017.
 */

public abstract class ReportUIConfig {

    Resources r;
    Context activityContext;
    LinearLayout innerLinearLayout;

    public ReportUIConfig(Context activityContext){
        this.r = activityContext.getResources();
        this.activityContext = activityContext;
        this.innerLinearLayout = getLinearLayout(activityContext);
    }

    public static LinearLayout getLinearLayout(Context context){

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        // Configuring the width and height of the linear innerLinearLayout.
        LinearLayout.LayoutParams llLP = new LinearLayout.LayoutParams(
                //android:layout_width
                LinearLayout.LayoutParams.MATCH_PARENT,
                //android:layout_height
                LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(llLP);

        return linearLayout;
    }

    public void setMarginsDp(View view, int left, int top, int right, int bottom){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );

        int pxLeft = dpToPx(left);
        int pxTop = dpToPx(top);
        int pxRight = dpToPx(right);
        int pxBottom = dpToPx(bottom);

        params.setMargins(pxLeft, pxTop, pxRight, pxBottom);
        view.setLayoutParams(params);
    }

    public int dpToPx(int dpValue){
        float d = r.getDisplayMetrics().density;
        return (int)(dpValue * d); // margin in pixels
    }

    public void addBlankView(int dpValue){
        View blank = getBlankView(dpValue);
        innerLinearLayout.addView(blank);
    }

    public View getBlankView(int dpValue){
        View blank = new View(activityContext);
        blank.setLayoutParams(new LinearLayout.LayoutParams(1, dpToPx(dpValue)));
        return blank;
    }


}
