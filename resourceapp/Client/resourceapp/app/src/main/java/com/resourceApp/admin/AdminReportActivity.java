package com.resourceApp.admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.SentReportActivity;
import com.resourceApp.model.Report;
import com.resourceApp.model.StatusUpdate;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.Utils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.Utils.allowOrientationChange;
import static com.resourceApp.utils.Utils.getProgressDialog;
import static com.resourceApp.utils.Utils.preventOrientationChange;
import static com.resourceApp.utils.Utils.showLongToastMessage;

/**
 * Created by Osman on 28/01/2017.
 */

public class AdminReportActivity extends SentReportActivity {

    TextView tvStatus;
    ImageView ivStatus;
    Switch sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initAdminHeader() {
        LinearLayout ll = (LinearLayout)findViewById(R.id.adminStatusHeader);
        ll.setVisibility(View.VISIBLE);

        int status = report.getStatusInt();
        tvStatus = (TextView)findViewById(R.id.tvStatusValue);
        ivStatus = (ImageView)findViewById(R.id.ivStatusIcon);
        sv = (Switch)findViewById(R.id.svStatus);

        tvStatus.setText(report.getStatus());
        if(status == 0) {
            Drawable statusIcon = getIcon(false);
            ivStatus.setImageDrawable(statusIcon);
            sv.setChecked(false);
        }

        sv.setOnCheckedChangeListener((compoundButton, b) -> {
            // Ensures the toggle button is actually pressed.
            if(!compoundButton.isPressed())
                return;
            Utils.preventOrientationChange(this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to change the status?");
            builder.setPositiveButton("Yes", (dialog, id) ->
                    promptStatusInfo(compoundButton, b));
            builder.setNegativeButton("Cancel", (dialog, id) ->
                    onCancelDialog(dialog, compoundButton, b));
            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.setCancelable(false);
            alert.show();
        });
    }

    public void promptStatusInfo(CompoundButton compoundButton, boolean b){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please provide a comment");

        final EditText et = getDialogEditText();
        FrameLayout container = getDialogContainer(et);
        builder.setView(container);

        builder.setPositiveButton("Submit", (dialog, id) -> {
            String comment = et.getText().toString();
            changeStatus(b, comment);
            Utils.allowOrientationChange(this);
        });
        builder.setNegativeButton("Cancel", (dialog, id) ->
                onCancelDialog(dialog, compoundButton, b));
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }

    public void changeStatus(boolean isChecked, String comment){
        String reportObjectId = report.getId();
        final boolean booleanFlag;
        final int newStatus;
        if(isChecked){
            booleanFlag = true;
            newStatus = 1;
        }else{
            booleanFlag = false;
            newStatus = 0;
        }

        /*
        Alternative method with comments.
         */
        String adminEmail = Utils.getSharedPrefs(this).getString(Constants.EMAIL,"");
        StatusUpdate statusUpdate = new StatusUpdate(adminEmail, comment, newStatus);

        ProgressDialog progressDialog = getProgressDialog(this);
        preventOrientationChange(this);

        MongoDbConn.getRetrofit().addReportStatus(reportObjectId, statusUpdate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        response -> {
                            /*
                            Updating toggle status and name
                             */
                            report.setStatus(newStatus);
                            String newStatusStr = report.getStatus();
                            tvStatus.setText(newStatusStr);
                            ivStatus.setImageDrawable(getIcon(booleanFlag));
                            /*
                            Updating reports object for when resolution history is selected
                            Without having to make an additional request.
                             */
                            ArrayList<StatusUpdate> statusUpdates = report.getStatusUpdates();
                            statusUpdates.add(statusUpdate);
                            report.setStatusUpdates(statusUpdates);
                            preparePostSync();
                            progressDialog.dismiss();
                            showLongToastMessage(this, "Status has changed to "+newStatusStr);
                            allowOrientationChange(this);
                        },
                        error -> {
                            sv.setChecked(!booleanFlag);
                            progressDialog.dismiss();
                            showLongToastMessage(this, "Status failed to change.");
                            allowOrientationChange(this);
                        }
                );
    }

    public void onCancelDialog(DialogInterface dialog, CompoundButton compoundButton, boolean isChecked){
        compoundButton.setChecked(!isChecked);
        dialog.cancel();
        Utils.allowOrientationChange(this);
    }

    public Drawable getIcon(boolean checked){
        if(checked)
            return getResources().getDrawable(R.drawable.ic_admin_open_report);
        else
            return getResources().getDrawable(R.drawable.ic_admin_close_report);
    }

    public void preparePostSync(){
        SharedPreferences sp = Utils.getSharedPrefs(this);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Constants.ADMIN_SYNC_PENDING, true);
        editor.apply();
    }

    public EditText getDialogEditText(){
        final EditText et = new EditText(this);
        et.setInputType(  InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                | InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        et.setGravity(Gravity.TOP | Gravity.LEFT);
        et.setMinLines(3);
        return et;
    }

    public FrameLayout getDialogContainer(EditText et){
        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        float d = this.getResources().getDisplayMetrics().density;
        int margin = (int)(15 * d);
        params.setMargins(margin,0,margin,0);
        et.setLayoutParams(params);
        container.addView(et);
        return container;
    }

}