package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Osman on 29/11/2016.
 */
public class SubFormField implements Parcelable {

    private String fieldTitle;
    private String widget;
    private ArrayList<String> options;

    public String getFieldTitle() {
        return fieldTitle;
    }

    public String getWidget() {
        return widget;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fieldTitle);
        dest.writeString(this.widget);
        dest.writeStringList(this.options);
    }

    public SubFormField() {
    }

    protected SubFormField(Parcel in) {
        this.fieldTitle = in.readString();
        this.widget = in.readString();
        this.options = in.createStringArrayList();
    }

    public static final Creator<SubFormField> CREATOR = new Creator<SubFormField>() {
        @Override
        public SubFormField createFromParcel(Parcel source) {
            return new SubFormField(source);
        }

        @Override
        public SubFormField[] newArray(int size) {
            return new SubFormField[size];
        }
    };
}
