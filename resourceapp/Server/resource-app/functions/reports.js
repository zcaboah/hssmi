'use strict';

const user = require('../models/user');
const report = require('../models/report');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const randomstring = require("randomstring");
const config = require('../config/config.json');

exports.getSentReports = userId => 
	
	new Promise((resolve,reject) => {
    
		report.find({ user_id: userId }).sort( { date_modified : -1 } )

		.then(reports => resolve(reports))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});

exports.sendReport = (reportJson) => 

	new Promise((resolve,reject) => {
        
        const newReport = new report({

            ref_no  		: reportJson.ref_no,
            report_title	: reportJson.report_title, 
            user_id         : reportJson.user_id,
            user_email      : reportJson.user_email,
            company_id     	: reportJson.company_id,
            sub_id     		: reportJson.sub_id,
            sub_title      	: reportJson.sub_title,
            date_modified   : reportJson.date_modified,
            sections        : reportJson.sections,
            status          : reportJson.status
		});
           
        newReport.save()

		.then(() => resolve({ status: 201, message: 'Report sent.' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }));
    
	});

exports.saveDraft = (email, reportJson) => 

	new Promise((resolve, reject) => {

		user.find({ email: email })

		.then(users => {

			let user = users[0];
			user.drafts.push(reportJson);
            
            return user.save();

		})

		.then(user => resolve({ status: 200, message: 'Draft saved.' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }));
    
	});

exports.updateDraft = (email, reportJson) => 

	new Promise((resolve, reject) => {
    
        const ref_no = reportJson.ref_no;

		user.update({ email: email, 'drafts.ref_no' : ref_no },
                   {"$set":{"drafts.$": reportJson }})

		.then(() => resolve({ status: 201, message: 'Draft saved.' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }));
    
	});

exports.deleteDrafts = (email, draftIds) =>

	new Promise((resolve, reject) => {
    
		user.update({ email: email },
                   {$pull : { 'drafts' : {'ref_no':{'$in': draftIds} } } })

		.then(() => resolve({ status: 201, message: 'Draft(s) deleted.' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }));
    
	});

exports.getOpenReports = companyId => 
	
	new Promise((resolve,reject) => {
    
		report.find({ company_id: companyId, status: 1 }).sort( { date_modified : -1 } )

		.then(reports => resolve(reports))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});

exports.getClosedReports = companyId => 
	
	new Promise((resolve,reject) => {
    
		report.find({ company_id: companyId, status: 0 }).sort( { date_modified : -1 } )

		.then(reports => resolve(reports))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});

exports.addReportStatus = (reportObjectId, statusUpdateObject) => 

	new Promise((resolve, reject) => {
        
		report.update({ _id: reportObjectId },
                   {$set : { 'status' : statusUpdateObject.status }}             
        )
        
        .then(() =>{ return report.update({ _id: reportObjectId },
                   {$push : { 'statusUpdates' : statusUpdateObject }});
        })

		.then(() => resolve({ status: 201, message: 'Status updated.' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }));
    
	});

exports.getReportStatusUpdates = reportObjectId => 
	
	new Promise((resolve,reject) => {
    
		report.find({ _id: reportObjectId }, { statusUpdates : 1, _id : 0 })

		.then(reports => resolve(reports[0].statusUpdates))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});