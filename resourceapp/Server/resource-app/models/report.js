'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reportSchema = mongoose.Schema({ 
    
	ref_no         	: String,
	report_title	: String, 
    user_id         : Schema.Types.ObjectId,
    user_email      : String,
	company_id     	: Schema.Types.ObjectId,
	sub_id     		: Schema.Types.ObjectId,
	sub_title      	: String,
    date_modified   : Date,
    sections        : Array,
    status          : Number,
    statusUpdates   : Array
});

mongoose.Promise = global.Promise;

mongoose.createConnection('mongodb://admin:abc12345@ds021046.mlab.com:21046/resource-app');
//mongoose.createConnection('localhost','resource-app');

module.exports = mongoose.model('report', reportSchema);        