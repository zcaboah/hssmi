'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = mongoose.Schema({ 

	name 			: String,
	email			: String, 
    user_level      : Number,
	hashed_password	: String,
	created_at		: String,
	temp_password	: String,
	temp_password_time: String,
    reports         : Array,
    drafts          : Array,
    company_id      : String
});

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://admin:abc12345@ds021046.mlab.com:21046/resource-app');
//mongoose.createConnection('localhost','resource-app');

module.exports = mongoose.model('user', userSchema);        