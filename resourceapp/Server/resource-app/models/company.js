'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const companySchema = mongoose.Schema({ 

	name 			: String,
	email			: String, 
	thumbnail       : Number
});

mongoose.Promise = global.Promise;

mongoose.createConnection('mongodb://admin:abc12345@ds021046.mlab.com:21046/resource-app');
//mongoose.createConnection('localhost','resource-app');

module.exports = mongoose.model('company', companySchema);        